package account

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrEmailMismatch occurs when the email to verify does not match the accounts email
	ErrEmailMismatch = errors.New(errors.EmailMismatch, "email does not match the accounts email")

	// ErrPermissionDenied occurs if an accessor tires to perform an action he isn't allowed to
	ErrPermissionDenied = errors.New(errors.PermissionDenied, "permission denied")

	// ErrNotFound occurs when a operation is performed on a non-existing account.
	ErrNotFound = errors.New(errors.NotFound, "account not found")

	// ErrAccountInfoIncomplete is returned if the params do not contain all the for the creation necessary data
	ErrAccountInfoIncomplete = errors.New(errors.NotEnoughInfo, "account info isn't complete")

	// ErrEmailAlreadyVerified occurs when the email to verify is already verified
	ErrEmailAlreadyVerified = errors.New(errors.EmailAlreadyVerified, "email already verified")

	// ErrEmailTaken is returned if a new account is created using a already used email
	ErrEmailTaken = errors.New(errors.EmailAlreadyTaken, "email is already used by another account")

	// ErrNicknameTaken is returned if a new account is created using a already used nickname
	ErrNicknameTaken = errors.New(errors.NicknameAlreadyTaken, "nickname is already used by another account")
)
