package account

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
	"strings"
	"time"

	"gitlab.com/barbra-group/validate"
)

// ProfilePrivacySetting represents an accounts privacy setting.
type ProfilePrivacySetting int

const (
	// Public profiles expose the accounts pictureURL, nickname, full Name, country, city, bio, nickname, birthday and what the account's owner knows about
	Public ProfilePrivacySetting = iota

	// Discrete profiles expose the accounts pictureURL, nickname, full Name, country, bio, nickname and what the account's owner knows about
	Discrete

	// Private profiles expose only the nickname and pictureURL to other users if the nickname isn't set the firstName will be visible.
	// Note that public Collections still are visible to other users
	Private
)

// Account is the resource representing a Barbra account.
type Account struct {
	ID                uuid.ID               `json:"id"`
	Nickname          string                `json:"nickname"`
	GivenName         string                `json:"given_name"`
	FamilyName        string                `json:"family_name"`
	Birthday          time.Time             `json:"birthday"`
	Bio               string                `json:"bio"`
	PictureURL        string                `json:"picture_url"`
	Address           Address               `json:"address"`
	KnowsAbout        []string              `json:"knows_about"`
	ProfileVisibility ProfilePrivacySetting `json:"profile_visibility"`
	Creation          time.Time             `json:"creation"`
	Email             string                `json:"email"`
	IsEmailVerified   bool                  `json:"is_email_verified"`
}

// Params are the parameters allowed during account creation/updates.
type Params struct {
	Email      *string
	Nickname   *string
	GivenName  *string
	FamilyName *string
	Birthday   *time.Time
	Bio        *string
	PictureURL *string
	KnowsAbout []string
	Address    *AddressParams
}

// Address describes common properties for an Address.
type Address struct {
	Country string `json:"country"`
	City    string `json:"city"`
	Street  string `json:"street"`
}

// AddressParams represents an address during account creation/updates.
type AddressParams struct {
	City    *string `json:"city"`
	Country *string `json:"country"`
	Street  *string `json:"street"`
}

// New creates a new account using the given params. Nil fields will be set to the default value. The email address will be normalized.
// The params will be validated.
func New(p Params, isEmailVerified bool) (*Account, error) {
	p.Normalize()

	if err := p.Validate(); err != nil {
		return nil, err
	}

	if p.Email == nil || p.GivenName == nil {
		return nil, ErrAccountInfoIncomplete
	}

	a := &Account{
		ID:              uuid.New(),
		Email:           strings.ToLower(*p.Email),
		GivenName:       *p.GivenName,
		Creation:        time.Now(),
		IsEmailVerified: isEmailVerified,
	}

	if p.Address != nil {
		if err := a.Address.Update(*p.Address); err != nil {
			return nil, err
		}
	}

	if p.Nickname != nil {
		a.Nickname = *p.Nickname
	}

	if p.FamilyName != nil {
		a.FamilyName = *p.FamilyName
	}

	if p.Birthday != nil {
		a.Birthday = *p.Birthday
	}

	if p.Bio != nil {
		a.Bio = *p.Bio
	}

	if p.PictureURL != nil {
		a.PictureURL = *p.PictureURL
	}

	if p.KnowsAbout != nil {
		a.KnowsAbout = p.KnowsAbout
	}

	return a, nil
}

// Update validates the params and updates the account. Only non-nil fields will be updated.
func (a *Account) Update(p Params) error {
	p.Normalize()

	if err := p.Validate(); err != nil {
		return err
	}

	if p.Address != nil {
		if err := a.Address.Update(*p.Address); err != nil {
			return err
		}
	}

	if p.Nickname != nil {
		a.Nickname = *p.Nickname
	}

	if p.GivenName != nil {
		a.GivenName = *p.GivenName
	}

	if p.FamilyName != nil {
		a.FamilyName = *p.FamilyName
	}

	if p.Birthday != nil {
		a.Birthday = *p.Birthday
	}

	if p.Bio != nil {
		a.Bio = *p.Bio
	}

	if p.PictureURL != nil {
		a.PictureURL = *p.PictureURL
	}

	if p.KnowsAbout != nil {
		a.KnowsAbout = p.KnowsAbout
	}

	return nil
}

// PublicInfo returns an account that only contains data that is allowed to be publicly visible according to the accounts ProfileVisibility.
func (a *Account) PublicInfo() *Account {
	switch a.ProfileVisibility {
	case Discrete:
		return &Account{
			ID:                a.ID,
			PictureURL:        a.PictureURL,
			GivenName:         a.GivenName,
			FamilyName:        a.FamilyName,
			Nickname:          a.Nickname,
			Bio:               a.Bio,
			ProfileVisibility: a.ProfileVisibility,
			KnowsAbout:        a.KnowsAbout,
			Address: Address{
				Country: a.Address.Country,
			},
		}

	case Public:
		return &Account{
			ID:                a.ID,
			PictureURL:        a.PictureURL,
			GivenName:         a.GivenName,
			FamilyName:        a.FamilyName,
			Nickname:          a.Nickname,
			Bio:               a.Bio,
			Birthday:          a.Birthday,
			ProfileVisibility: a.ProfileVisibility,
			KnowsAbout:        a.KnowsAbout,
			Address: Address{
				Country: a.Address.Country,
				City:    a.Address.City,
			},
		}

	default:
		return &Account{
			ID:                a.ID,
			PictureURL:        a.PictureURL,
			Nickname:          a.Nickname,
			GivenName:         a.GivenName,
			ProfileVisibility: a.ProfileVisibility,
		}
	}
}

// DisplayName returns the accounts display name (for use in emails etc.)
func (a *Account) DisplayName() string {
	if a.Nickname != "" {
		return a.Nickname
	}

	return a.GivenName
}

// Validate validates all fields and returns an error if a field isn't valid
func (p *Params) Validate() error {
	return validate.Fields("account data",
		validate.StringPtr(p.Email, "email", true, false, validate.IsEmail, validate.IsLowerCase),
		validate.StringPtr(p.Nickname, "nickname", true, true, validate.HasRuneLength(1, 50), validate.IsAlphaUnicodeNumeric),
		validate.StringPtr(p.GivenName, "given_name", true, false, validate.HasRuneLength(1, 50), validate.IsAlphaUnicode),
		validate.StringPtr(p.FamilyName, "family_name", true, true, validate.HasRuneLength(1, 50), validate.IsAlphaUnicode),
		validate.TimePtr(p.Birthday, "birthday", true, true, validate.IsOlderThan(16)),
		validate.StringPtr(p.Bio, "bio", true, true, validate.HasRuneLength(1, 5000)),
		validate.StringPtr(p.PictureURL, "picture_url", true, true, validate.IsURL),
		validate.Validateable(p.Address, "address_data", true),
		validate.StringSlice(p.KnowsAbout, "knows_about", true, validate.AllStringRules(validate.HasRuneLength(2, 30), validate.IsAlphaUnicodeNumeric)),
	)
}

// Normalize normalizes the account params (currently is just makes the email lowercase)
func (p *Params) Normalize() {
	if p.Email != nil {
		*p.Email = strings.ToLower(*p.Email)
	}
}

// Update validates and updates address. Only non-nil fields will be updated.
func (a *Address) Update(p AddressParams) error {
	if err := p.Validate(); err != nil {
		return err
	}

	if p.Country != nil {
		a.Country = *p.Country
	}

	if p.City != nil {
		a.City = *p.City
	}

	if p.Street != nil {
		a.Street = *p.Street
	}

	return nil
}

// Validate validates all fields and returns an error if a field isn't valid
func (a *AddressParams) Validate() error {
	return validate.Fields("address_params",
		validate.StringPtr(a.City, "city", true, true, validate.HasRuneLength(1, 300)),
		validate.StringPtr(a.Street, "address", true, true, validate.HasRuneLength(1, 300)),
		validate.StringPtr(a.Street, "street", true, true, validate.HasRuneLength(1, 300)),
	)
}
