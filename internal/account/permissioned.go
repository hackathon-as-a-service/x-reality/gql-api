package account

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// permissionedService provides a security aware wrapper around Service.
type permissionedService struct {
	next     Service
	accessor permission.Accessor
}

// NewPermissionedService creates a new permissionedService.
func NewPermissionedService(next Service, accessor permission.Accessor) Service {
	return &permissionedService{
		next:     next,
		accessor: accessor,
	}
}

// Create provides a security aware wrapper around service.Create which doesn't allows accounts to be created directly.
func (s *permissionedService) Create(p Params, isEmailVerified bool) (*Account, error) {
	return nil, ErrPermissionDenied
}

// Get provides a security aware wrapper around service.Get which allows the user to see his full account info. If the accessor isn't the account owner,
// only the accounts public info will returned.
func (s *permissionedService) Get(id uuid.ID) (*Account, error) {
	if !s.accessor.Is(id) {
		return s.next.GetPublic(id)
	}

	return s.next.Get(id)
}

// GetByNickname provides a security aware wrapper around service.GetByNickname which allows the user to see his full account info. If the accessor isn't the account owner,
// only the accounts public info will returned.
func (s *permissionedService) GetByNickname(nick string) (*Account, error) {
	acc, err := s.next.GetByNickname(nick)
	if err != nil {
		return nil, err
	}

	if !s.accessor.Is(acc.ID) {
		return acc.PublicInfo(), nil
	}

	return acc, nil
}

// GetByEmail provides a security aware wrapper around service.GetByEmail which allows the user to see his full account info. If the accessor isn't the account owner,
// only the accounts public info will returned.
func (s *permissionedService) GetByEmail(email string) (*Account, error) {
	acc, err := s.next.GetByEmail(email)
	if err != nil {
		return nil, err
	}

	if !s.accessor.Is(acc.ID) {
		return acc.PublicInfo(), nil
	}

	return acc, nil
}

// GetByEmail provides a security aware wrapper around service.GetPublic which just calls service.GetPublic.
func (s *permissionedService) GetPublic(id uuid.ID) (*Account, error) {
	return s.next.GetPublic(id)
}

// Update provides a security aware wrapper around service.Update which allows the account owner to update his account.
func (s *permissionedService) Update(id uuid.ID, p Params) (*Account, error) {
	if !s.accessor.Is(id) {
		return nil, ErrPermissionDenied
	}

	return s.next.Update(id, p)
}

// VerifyEmail provides a security aware wrapper around service.VerifyEmail which just calls service.VerifyEmail.
func (s *permissionedService) VerifyEmail(token string) (*Account, error) {
	return s.next.VerifyEmail(token)
}
