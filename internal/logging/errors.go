package logging

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
)

var (
	// ErrInvalidLoggingLevel is returned if no logging level with the given name exists.
	ErrInvalidLoggingLevel = errors.New(errors.Unknown, "invalid logging level")
)
