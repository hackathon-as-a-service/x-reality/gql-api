package config

// Config represents the application configuration.
type Config struct {
	Port               int      `default:"8001"`
	URL                string   `default:"api.barbra.io"`
	Domain             string   `default:"barbra.io"`
	SessionSecret      string   `required:"true" envconfig:"session_secret"`
	LoggingLevel       string   `default:"debug" envconfig:"logging_level"`
	CORSAllowedOrigins []string `envconfig:"cors_allowed_origins"`

	LoginRedirectURL   string `default:"barbra.io/user/me" envconfig:"login_redirect_url"`
	ErrorRedirectURL   string `default:"barbra.io/error" envconfig:"error_redirect_url"`
	EmailActivationURL string `default:"barbra.io/confirm-email-address" envconfig:"email_activation_url"`

	Couchbase struct {
		URL      string `required:"true" envconfig:"couchbase_url"`
		User     string `required:"true" envconfig:"couchbase_username"`
		Password string `required:"true" envconfig:"couchbase_password"`
		Bucket   string `required:"true" envconfig:"couchbase_bucket"`
	}

	Extauth struct {
		Google struct {
			ClientID string `required:"true" envconfig:"extauth_google_clientid"`
			Secret   string `required:"true" envconfig:"extauth_google_secret"`
		}
	}
}
