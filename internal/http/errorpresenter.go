package http

import (
	"fmt"
	"net/http"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
)

// ErrorPresenterFunc represents a http error presenter function
type ErrorPresenterFunc = func(w http.ResponseWriter, r *http.Request, code errors.Code)

// ErrorHandler is used to handle http errors
type ErrorHandler struct {
	presenter ErrorPresenterFunc
	log       logging.Logger
}

// NewErrorHandler creates a new ErrorHandler
func NewErrorHandler(log logging.Logger, presenter ErrorPresenterFunc) *ErrorHandler {
	return &ErrorHandler{
		log:       log,
		presenter: presenter,
	}
}

// Handle logs the given error and calls the error presenter
func (h *ErrorHandler) Handle(w http.ResponseWriter, r *http.Request, err error) {
	code := errors.Unknown
	if err, ok := err.(*errors.Error); ok {
		code = err.Code()
	}

	if code == errors.Unknown {
		h.log.Errorw(err.Error(), "code", code, "err", err)
	} else {
		h.log.Infow(err.Error(), "code", code)
	}

	h.presenter(w, r, code)
}

// NewRedirectErrorPresenter creates a new http error presenter which redirects the requester the specified error page with the code query param set to the
// error code
func NewRedirectErrorPresenter(redirectURL string) ErrorPresenterFunc {
	return func(w http.ResponseWriter, r *http.Request, c errors.Code) {
		http.Redirect(w, r, fmt.Sprintf("%v?code=%d", redirectURL, c), http.StatusTemporaryRedirect)
	}
}

// NewStatusCodeErrorPresenter creates a new http error presenter which sets the request status to the HTTP status obtained by the
// error code
func NewStatusCodeErrorPresenter() ErrorPresenterFunc {
	return func(w http.ResponseWriter, r *http.Request, c errors.Code) {
		w.WriteHeader(c.HTTPStatus())
	}
}
