package handler

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"
)

var (
	// ErrIncompleteRequestData is returned if the request does not contain all for the action necessary data
	ErrIncompleteRequestData = errors.New(errors.NotEnoughInfo, "request does not contain all necessary data")
)
