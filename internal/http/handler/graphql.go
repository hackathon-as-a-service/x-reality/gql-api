package handler

import (
	"github.com/99designs/gqlgen/handler"
	"github.com/go-chi/chi"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/graph"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/reqestcontext"
	internalHttp "gitlab.com/gett-hackathon/x-reality/gql-api/internal/http"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/session"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

	"net/http"
)

const (
	// GraphQLEndpointRoute is the http route used to handle graphql requests.
	GraphQLEndpointRoute = "/graphql"

	// GraphQLPlaygroundRoute is the http route used to serve the graphql playground.
	GraphQLPlaygroundRoute = "/playground"
)

// GraphQL is the GraphQL http handler
type GraphQL struct {
	handleFunc           http.HandlerFunc
	sessionSrv           session.Service
	errHandler           *internalHttp.ErrorHandler
	injector             *rctx.Injector
	playGroundHandleFunc http.HandlerFunc
}

// NewGraphQL creates a new http graphql handler.
func NewGraphQL(resolverRoot graph.ResolverRoot, sessionSrv session.Service, injector *rctx.Injector, log logging.Logger) *GraphQL {
	return &GraphQL{
		playGroundHandleFunc: handler.Playground("Barbra API Playground", GraphQLEndpointRoute),
		errHandler:           internalHttp.NewErrorHandler(log, internalHttp.NewStatusCodeErrorPresenter()),
		sessionSrv:           sessionSrv,
		handleFunc:           handler.GraphQL(graph.NewExecutableSchema(graph.Config{Resolvers: resolverRoot})),
		injector:             injector,
	}
}

// RegisterRoutes registers the handlers routes on the given router
func (h *GraphQL) RegisterRoutes(r chi.Router) {
	r.Get(GraphQLEndpointRoute, h.GraphQL)
	r.Post(GraphQLEndpointRoute, h.GraphQL)
	r.Get(GraphQLPlaygroundRoute, h.Playground)
}

// GraphQL handles GraphQL endpoint requests.
func (h *GraphQL) GraphQL(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get("Authorization")
	if token == "" {
		c, err := r.Cookie(SessionName)
		if err == nil {
			token = c.Value
		}
	}

	accessorID := permission.UnauthorizedID
	if token != "" {
		s := &session.Session{}
		if err := h.sessionSrv.Parse(s, token); err != nil {
			h.errHandler.Handle(w, r, err)
			return
		}

		id, err := uuid.FromString(s.Subject)
		if err != nil {
			h.errHandler.Handle(w, r, err)
			return
		}

		accessorID = id
	}

	h.handleFunc(w, r.WithContext(h.injector.Inject(r.Context(), permission.NewAccessor(accessorID))))
}

// Playground handles GraphQL playground requests.
func (h *GraphQL) Playground(w http.ResponseWriter, r *http.Request) {
	h.playGroundHandleFunc(w, r)
}
