package couchbase

import (
	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// NewAppRepository creates a new couchbase based app.Repository.
func NewAppRepository(bucket *gocb.Bucket) app.Repository {
	return &appRepository{
		bucket: bucket,
	}
}

type appRepository struct {
	bucket *gocb.Bucket
}

func (r *appRepository) Get(id uuid.ID) (*app.App, error) {
	a := &Document{Base: &app.App{}}
	if _, err := r.bucket.Get(SerializeKey(AppType, id.String()), a); err != nil {
		return nil, r.toInternalErr(err)
	}

	return a.Base.(*app.App), nil
}

// CREATE INDEX idx3 ON xstore(type, base.`public`, base.owner_id, DISTINCT ARRAY base.contributors FOR c IN base.contributors END) USING GSI;
func (r *appRepository) GetAll(accountID uuid.ID) ([]*app.App, error) {
	return r.n1qlQuery(gocb.NewN1qlQuery("SELECT * FROM `xstore` WHERE type = 'app' AND (base.`public` = true OR base.owner_id = $1 OR ANY c IN base.contributors SATISFIES o = $1 END);"), []interface{}{accountID.String()})
}

func (r *appRepository) GetAllOwned(accountID uuid.ID) ([]*app.App, error) {
	return r.n1qlQuery(gocb.NewN1qlQuery("SELECT * FROM `xstore` WHERE type = 'app' AND base.owner_id = $1;"), []interface{}{accountID.String()})
}

func (r *appRepository) GetSharedApps(accountID uuid.ID) ([]*app.App, error) {
	return r.n1qlQuery(gocb.NewN1qlQuery("SELECT * FROM `xstore` WHERE type = 'app' AND (base.owner_id = $1 OR ANY c IN base.contributors SATISFIES o = $1 END);"), []interface{}{accountID.String()})
}

func (r *appRepository) GetByCategory(accountID uuid.ID, category app.Category) ([]*app.App, error) {
	return r.n1qlQuery(gocb.NewN1qlQuery("SELECT * FROM `xstore` WHERE type = 'app' AND (base.`public` = true OR base.owner_id = $1 OR ANY c IN base.contributors SATISFIES o = $1 END) AND base.category = $2;"), []interface{}{accountID.String(), category.String()})
}

func (r *appRepository) Insert(a *app.App) error {
	_, err := r.bucket.Insert(SerializeKey(AppType, a.ID.String()), NewDocument(AppType, 0, a), 0)
	return r.toInternalErr(err)
}

func (r *appRepository) Update(a *app.App) error {
	_, err := r.bucket.Replace(SerializeKey(AppType, a.ID.String()), NewDocument(AppType, 0, a), 0, 0)
	return r.toInternalErr(err)
}

func (r *appRepository) n1qlQuery(query *gocb.N1qlQuery, params []interface{}) ([]*app.App, error) {
	res, err := r.bucket.ExecuteN1qlQuery(query, params)

	if err != nil {
		return nil, err
	}

	var apps []*app.App
	a := &XRealityStoreResult{XStore: &Document{Base: &app.App{}}}
	for res.Next(a) {
		apps = append(apps, a.XStore.(*Document).Base.(*app.App))
		a = &XRealityStoreResult{XStore: &Document{Base: &app.App{}}}
	}

	return apps, nil
}

func (r *appRepository) toInternalErr(err error) error {
	switch err {
	default:
		return err
	}
}
