package couchbase

import (
	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/extauth"
)

// extauthRepository is a basic, couchbase based implementation of extauth.Repository
type extauthRepository struct {
	bucket *gocb.Bucket
}

// NewExtauthRepository creates a new extauth repository (this repo isn't dataloaded!).
func NewExtauthRepository(bucket *gocb.Bucket) extauth.Repository {
	return &extauthRepository{
		bucket: bucket,
	}
}

func (r *extauthRepository) GetOAuthAccount(provider, sub string) (*extauth.Account, error) {
	doc := &Document{Base: &extauth.Account{}}
	if _, err := r.bucket.Get(SerializeKey(ExtauthType, provider, sub), doc); err != nil {
		return nil, r.toInternalErr(err)
	}

	return doc.Base.(*extauth.Account), nil
}

func (r *extauthRepository) InsertExtauthAccount(authAcc *extauth.Account) error {
	_, err := r.bucket.Insert(SerializeKey(ExtauthType, authAcc.Provider, authAcc.Subject), NewDocument(ExtauthType, 1, authAcc), 0)
	return r.toInternalErr(err)
}

func (r *extauthRepository) UpdateExtauthAccountAccount(authAcc *extauth.Account) error {
	_, err := r.bucket.Replace(SerializeKey(ExtauthType, authAcc.Provider, authAcc.Subject), NewDocument(ExtauthType, 1, authAcc), 0, 0)
	return r.toInternalErr(err)
}

func (r *extauthRepository) toInternalErr(err error) error {
	switch err {
	case gocb.ErrKeyNotFound:
		return extauth.ErrNotFound
	default:
		return err
	}
}
