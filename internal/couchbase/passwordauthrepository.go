package couchbase

import (
	"github.com/couchbase/gocb"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/passwordauth"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

const (
	// PasswordAuthDocumentType is the document type name of password auth informations
	PasswordAuthDocumentType = "extauth"
)

// passwordAuthRepository is a basic, couchbase based implementation of passwordauth.Repository
type passwordAuthRepository struct {
	bucket *gocb.Bucket
}

// NewPasswordAuthRepository creates a new passwordauth repository.
func NewPasswordAuthRepository(bucket *gocb.Bucket) passwordauth.Repository {
	return &passwordAuthRepository{
		bucket: bucket,
	}
}

func (r *passwordAuthRepository) GetInfo(ownerID uuid.ID) (*passwordauth.Info, error) {
	doc := &Document{Base: &passwordauth.Info{}}
	if _, err := r.bucket.Get(SerializeKey(PasswordAuthDocumentType, ownerID.String()), doc); err != nil {
		return nil, r.toInternalErr(err)
	}

	return doc.Base.(*passwordauth.Info), nil
}

func (r *passwordAuthRepository) InsertInfo(i *passwordauth.Info) error {
	_, err := r.bucket.Insert(SerializeKey(PasswordAuthDocumentType, i.OwnerID.String()), NewDocument(PasswordAuthDocumentType, 1, i), 0)
	return r.toInternalErr(err)
}

func (r *passwordAuthRepository) UpdateInfo(i *passwordauth.Info) error {
	_, err := r.bucket.Replace(SerializeKey(PasswordAuthDocumentType, i.OwnerID.String()), NewDocument(PasswordAuthDocumentType, 1, i), 0, 0)
	return r.toInternalErr(err)
}

func (r *passwordAuthRepository) toInternalErr(err error) error {
	switch err {
	case gocb.ErrKeyNotFound:
		return passwordauth.ErrNotFound
	default:
		return err
	}
}
