package couchbase

import (
	"strings"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// IDSeparator is the separator used to separate document id parts
const IDSeparator = ":"

// LookupKey is used to represent lookup keys inside a database.
type LookupKey struct {
	Key uuid.ID `json:"key"`
}

// CountResult is used to deserialize Count() n1ql queries.
type CountResult struct {
	Count int `json:"count"`
}

type XRealityStoreResult struct {
	XStore interface{} `json:"xstore"`
}

// SerializeKey serializes the given key parts into a key string.
func SerializeKey(docType DocumentType, parts ...string) string {
	return strings.Join(append([]string{string(docType)}, parts...), IDSeparator)
}

// GetKeyType returns the keys document type
func GetKeyType(key string) DocumentType {
	return DocumentType(strings.SplitN(key, IDSeparator, 2)[0])
}
