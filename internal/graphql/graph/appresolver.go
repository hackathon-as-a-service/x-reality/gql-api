package graph

import (
	"context"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/graphql/model"
)

type appResolver struct {
}

func NewAppResolver() AppResolver {
	return &appResolver{}
}

func (r *appResolver) DownloadUrls(ctx context.Context, obj *app.App) (model.DownloadURL, error) {
	return model.DownloadURL{
		Google:    &obj.DownloadURLs.Google,
		Microsoft: &obj.DownloadURLs.Microsoft,
		Vive:      &obj.DownloadURLs.Vive,
		Oculus:    &obj.DownloadURLs.Oculus,
	}, nil
}

func (r *appResolver) Category(ctx context.Context, obj *app.App) (*string, error) {
	s := obj.Category.String()
	return &s, nil
}
