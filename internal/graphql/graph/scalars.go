package graph

import (
	"errors"
	"io"
	"strconv"
	"time"

	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/app"

	"github.com/99designs/gqlgen/graphql"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

// MarshalDate is the date scalar marshaller.
func MarshalDate(t time.Time) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.FormatInt(t.Unix(), 10))
	})
}

// UnmarshalDate is the date scalar un-marshaller.
func UnmarshalDate(v interface{}) (time.Time, error) {
	if i, ok := v.(int); ok {
		return time.Unix(int64(i), 0), nil
	}

	return time.Time{}, errors.New("date should be a unix timestamp")
}

// MarshalID is the custom id scalar marshaller.
func MarshalID(id uuid.ID) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.Quote(id.String()))
	})
}

// UnmarshalID is the custom id scalar un-marshaller.
func UnmarshalID(v interface{}) (uuid.ID, error) {
	if s, ok := v.(string); ok {
		id, err := uuid.FromString(s)
		if err != nil {
			return "", err
		}

		return id, nil
	}

	return "", errors.New("id should be a string")
}

func MarshalCategory(c app.Category) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, strconv.Quote(c.String()))
	})
}

func UnmarshalCategory(i interface{}) (app.Category, error) {
	if s, ok := i.(string); ok {
		c, err := app.ToCategory(s)
		if err != nil {
			return 0, err
		}

		return c, nil
	}

	return 0, errors.New("category should be a string")
}
