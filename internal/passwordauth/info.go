package passwordauth

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
	"golang.org/x/crypto/bcrypt"
)

// Info is used to allow password login to a specific account
type Info struct {
	PasswordHash []byte
	OwnerID      uuid.ID
}

// NewInfo creates a new authentication info
func NewInfo(accID uuid.ID, pw string) (*Info, error) {
	i := &Info{
		OwnerID: accID,
	}

	if err := i.SetPassword(pw); err != nil {
		return nil, err
	}

	return i, nil
}

// Verify verifies the account info's password
func (i *Info) Verify(pw string) error {
	if len(i.PasswordHash) == 0 {
		return ErrPasswordNotSet
	}

	err := bcrypt.CompareHashAndPassword(i.PasswordHash, []byte(pw))
	if err == bcrypt.ErrMismatchedHashAndPassword {
		return ErrWrongPassword
	}

	return err
}

// SetPassword sets the authentication info's password
func (i *Info) SetPassword(pw string) error {
	if len(pw) < 7 {
		return ErrPasswordTooShort
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(pw), 13)
	if err != nil {
		return err
	}

	i.PasswordHash = hash
	return nil
}
