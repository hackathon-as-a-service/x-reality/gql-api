package passwordauth

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

// Repository provides the basic database CRUD operations used by Service.
type Repository interface {
	// GetInfo returns the auth info with the account ID.
	// If the account isn't found an error will be returned.
	GetInfo(ownerID uuid.ID) (*Info, error)

	// InsertInfo inserts a new auth info into the database.
	InsertInfo(i *Info) error

	// UpdateInfo updates the given auth info (It will be identified by the ownerID).
	UpdateInfo(i *Info) error
}
