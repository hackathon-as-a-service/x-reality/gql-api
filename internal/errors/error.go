package errors

// Error is used to represent a barbra application error
type Error struct {
	msg  string
	code Code
}

// New creates a new Error
func New(code Code, msg string) error {
	return &Error{msg, code}
}

// Wrap creates a new Error that wraps the previous one with an error code
func Wrap(err Error, code Code) error {
	return &Error{err.Error(), code}
}

// Error returns the errors msg
func (e *Error) Error() string {
	return e.msg
}

// Code returns the errors code
func (e *Error) Code() Code {
	return e.code
}

// Errors is used to combine multiple errors into one
type Errors []error

// Combine combines multiple errors into a single Errors object. Nil errors will be skipped. If all errors are nil, nil will be returned.
func Combine(errs ...error) Errors {
	var ee []error
	for _, e := range errs {
		if e != nil {
			ee = append(ee, e)
		}
	}

	return Errors(ee)
}

// Error returns the combined error msg of all errors inside Errors
func (e Errors) Error() string {
	if len(e) == 0 {
		return ""
	}

	msg := e[0].Error()
	for i := 1; i < len(e); i++ {
		msg += "; " + e[i].Error()
	}

	return msg
}
