package errors

import (
	"net/http"
)

// Code represents a barbra error code used by the client to identify the type of error
type Code int

const (
	// Unknown error code (documented in the readme)
	Unknown Code = 0

	// NotFound error code (documented in the readme)
	NotFound Code = 11

	// AlreadyExists error code (documented in the readme)
	AlreadyExists Code = 12

	// PermissionDenied error code (documented in the readme)
	PermissionDenied Code = 20

	// EmailAlreadyTaken error code (documented in the readme)
	EmailAlreadyTaken Code = 101

	// NicknameAlreadyTaken error code (documented in the readme)
	NicknameAlreadyTaken Code = 102

	// PasswordToShort error code (documented in the readme)
	PasswordToShort Code = 103

	// ExtauthAccountAlreadyRegistered error code (documented in the readme)
	ExtauthAccountAlreadyRegistered Code = 110

	// NotEnoughInfo error code (documented in the readme)
	NotEnoughInfo Code = 111

	// PasswordNotSet error code (documented in the readme)
	PasswordNotSet Code = 121

	// EmailAlreadyVerified error code (documented in the readme)
	EmailAlreadyVerified Code = 131

	// EmailMismatch error code (documented in the readme)
	EmailMismatch Code = 132

	// SessionExpired error code (documented in the readme)
	SessionExpired Code = 141

	// UnexpectedSigningMethod error code (documented in the readme)
	UnexpectedSigningMethod Code = 142

	// InvalidToken error code (documented in the readme)
	InvalidToken Code = 143
)

// String returns the to the error code belonging message
func (c Code) String() string {
	switch c {
	case NotFound:
		return "not found"
	case AlreadyExists:
		return "already exists"
	case PermissionDenied:
		return "permission denied"
	case EmailAlreadyTaken:
		return "email is already taken"
	case NicknameAlreadyTaken:
		return "nickname is already taken"
	case PasswordToShort:
		return "password is to short"
	case ExtauthAccountAlreadyRegistered:
		return "extauth account is already in use"
	case NotEnoughInfo:
		return "not enough information"
	case PasswordNotSet:
		return "password isn't set"
	case EmailAlreadyVerified:
		return "email is already verified"
	case SessionExpired:
		return "session expired"
	case UnexpectedSigningMethod:
		return "unexpected token signing method"
	case InvalidToken:
		return "invalid token"
	default:
		return "internal server error"
	}
}

// HTTPStatus returns the to the error code belonging http status code
func (c Code) HTTPStatus() int {
	switch c {
	case NotFound:
		return http.StatusNotFound
	case AlreadyExists:
		return http.StatusConflict
	case PermissionDenied:
		return http.StatusUnauthorized
	case EmailAlreadyTaken:
		return http.StatusConflict
	case NicknameAlreadyTaken:
		return http.StatusConflict
	case PasswordToShort:
		return http.StatusBadRequest
	case ExtauthAccountAlreadyRegistered:
		return http.StatusConflict
	case NotEnoughInfo:
		return http.StatusBadRequest
	case PasswordNotSet:
		return http.StatusUnauthorized
	case EmailAlreadyVerified:
		return http.StatusAlreadyReported
	case SessionExpired:
		return http.StatusUnauthorized
	case UnexpectedSigningMethod:
		return http.StatusBadRequest
	case InvalidToken:
		return http.StatusBadRequest
	default:
		return http.StatusInternalServerError
	}
}
