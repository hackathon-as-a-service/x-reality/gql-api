package app

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
)

type App struct {
	ID           uuid.ID      `json:"id"`
	Public       bool         `json:"public"`
	Contributors []uuid.ID    `json:"contributors"`
	Name         string       `json:"name"`
	OwnerID      uuid.ID      `json:"owner_id"`
	Description  string       `json:"description"`
	Category     Category     `json:"category"`
	DownloadURLs DownloadURLs `json:"download_urls"`
	PreviewURLs  []string     `json:"preview_urls"`
	LogoURL      string       `json:"logo_url"`
	Tags         []string     `json:"tags"`
}

type DownloadURLs struct {
	Vive      string
	Oculus    string
	Microsoft string
	Google    string
}

type Params struct {
	Public       *bool
	Name         *string
	OwnerID      *uuid.ID
	Description  *string
	Contributors []uuid.ID
	Category     *Category
	DownloadURLs *DownloadURLParams
	LogoURL      *string
	PreviewURLs  []string
	Tags         []string
}

type DownloadURLParams struct {
	Vive      *string
	Oculus    *string
	Microsoft *string
	Google    *string
}

// TODO: Validation
func New(p Params) (*App, error) {
	app := &App{
		ID: uuid.New(),
	}

	if p.Name != nil {
		app.Name = *p.Name
	}

	if p.OwnerID != nil {
		app.OwnerID = *p.OwnerID
	}

	if p.Description != nil {
		app.Description = *p.Description
	}

	if p.Category != nil {
		app.Category = *p.Category
	}

	if p.DownloadURLs != nil {
		app.DownloadURLs.Update(*p.DownloadURLs)
	}

	if p.LogoURL != nil {
		app.LogoURL = *p.LogoURL
	}

	if p.Tags != nil {
		app.Tags = p.Tags
	}

	if p.Public != nil {
		app.Public = *p.Public
	}

	if p.Contributors != nil {
		app.Contributors = p.Contributors
	}

	if p.PreviewURLs != nil {
		app.PreviewURLs = p.PreviewURLs
	}

	return app, nil
}

// TODO: Validation
func (a *App) Update(p Params) error {
	if p.Name != nil {
		a.Name = *p.Name
	}

	if p.OwnerID != nil {
		a.OwnerID = *p.OwnerID
	}

	if p.Description != nil {
		a.Description = *p.Description
	}

	if p.Category != nil {
		a.Category = *p.Category
	}

	if p.DownloadURLs != nil {
		a.DownloadURLs.Update(*p.DownloadURLs)
	}

	if p.LogoURL != nil {
		a.LogoURL = *p.LogoURL
	}

	if p.Tags != nil {
		a.Tags = p.Tags
	}

	if p.Public != nil {
		a.Public = *p.Public
	}

	if p.Contributors != nil {
		a.Contributors = p.Contributors
	}

	if p.PreviewURLs != nil {
		a.PreviewURLs = p.PreviewURLs
	}

	return nil
}

func (a *App) CanView(id uuid.ID) bool {
	if a.Public {
		return true
	}

	if a.OwnerID == id {
		return true
	}

	for _, contribID := range a.Contributors {
		if contribID == id {
			return true
		}
	}

	return false
}

func (u *DownloadURLs) Update(p DownloadURLParams) {
	if p.Vive != nil {
		u.Vive = *p.Vive
	}

	if p.Google != nil {
		u.Google = *p.Google
	}

	if p.Microsoft != nil {
		u.Microsoft = *p.Microsoft
	}

	if p.Oculus != nil {
		u.Oculus = *p.Oculus
	}
}
