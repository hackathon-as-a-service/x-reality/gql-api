package app

type Vendor int

const (
	Vive = iota
	Google
	Oculus
	Microsoft
)

var (
	vendor = []string{"vive", "google", "oculus", "ms"}
)

func ToVendor(s string) (Vendor, error) {
	for i, category := range categories {
		if category == s {
			return Vendor(i), nil
		}
	}

	return 0, ErrCategoryNotFound
}

func (v Vendor) String() string {
	return categories[int(v)]
}
