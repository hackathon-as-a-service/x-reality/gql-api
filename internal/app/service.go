package app

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

type Service interface {
	Get(id uuid.ID) (*App, error)

	GetAll(accountID uuid.ID) ([]*App, error)
	GetAllOwned(accountID uuid.ID) ([]*App, error)
	GetSharedApps(accountID uuid.ID) ([]*App, error)
	GetByCategory(accountID uuid.ID, category Category) ([]*App, error)

	Create(p Params) (*App, error)
	Update(id uuid.ID, p Params) (*App, error)
}

func NewService(repo Repository) Service {
	return &service{
		repo: repo,
	}
}

type service struct {
	repo Repository
}

func (s *service) GetAllOwned(accountID uuid.ID) ([]*App, error) {
	return s.repo.GetAllOwned(accountID)
}

func (s *service) Get(id uuid.ID) (*App, error) {
	return s.repo.Get(id)
}

func (s *service) GetAll(accountID uuid.ID) ([]*App, error) {
	return s.repo.GetAll(accountID)
}

func (s *service) GetSharedApps(accountID uuid.ID) ([]*App, error) {
	return s.repo.GetSharedApps(accountID)
}

func (s *service) GetByCategory(accountID uuid.ID, category Category) ([]*App, error) {
	return s.repo.GetByCategory(accountID, category)
}

func (s *service) Create(p Params) (*App, error) {
	app, err := New(p)
	if err != nil {
		return nil, err
	}

	return app, s.repo.Insert(app)
}

func (s *service) Update(id uuid.ID, p Params) (*App, error) {
	app, err := s.repo.Get(id)
	if err != nil {
		return nil, err
	}

	if err := app.Update(p); err != nil {
		return nil, err
	}

	return app, s.repo.Update(app)
}
