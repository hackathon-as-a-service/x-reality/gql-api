package extauth

// Repository provides the basic database CRUD operations used by Service.
type Repository interface {
	// GetBySocialLogin returns the Account with the given provider, subject.
	// If the account isn't found an error will be returned.
	GetOAuthAccount(provider, sub string) (*Account, error)

	// InsertOAuthAccount inserts a new oauth account into the database.
	InsertExtauthAccount(authAcc *Account) error

	// UpdateExtauthAccountAccount updates the given oauth account (It will be identified by the provider and subject).
	UpdateExtauthAccountAccount(authAcc *Account) error
}
