package extauth

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/account"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
)

// Service provides the logic necessary to provide login/registration using external auth providers (google, facebook etc.).
type Service interface {
	// Register registers a new account using the given oauth provider and code. The oauth account will be linked to the account if the registration is successful.
	// All for the registration required data will be fetched from the provider. If the account is already registered, the accounts id will just be returned.
	// If the registration is success the accounts id will be returned.
	Register(provider, code string) (uuid.ID, error)

	// Login logs a user in using the given oauth provider and code.
	// If the login is successful the accounts id will be returned.
	Login(provider, code string) (uuid.ID, error)

	// AuthCodeURL generates a new extauth AuthUrl for the given provider (e.g. google) with the given state.
	AuthCodeURL(provider, state string) (string, error)

	// RandomState generates a new random extauth state.
	RandomState() (string, error)
}

type service struct {
	manager *multiauth.Manager
	accSrv  account.Service
	repo    Repository
}

// NewService creates a new extauth service
func NewService(repo Repository, manager *multiauth.Manager, accSrv account.Service) Service {
	return &service{
		manager: manager,
		repo:    repo,
		accSrv:  accSrv,
	}
}

// Login logs a user in using the given oauth provider and code.
// If the login is successful the accounts id will be returned.
func (s *service) Login(provider, code string) (uuid.ID, error) {
	info, err := s.getUserInfo(provider, code)
	if err != nil {
		return "", err
	}

	authAcc, err := s.repo.GetOAuthAccount(provider, info.Subject)
	if err != nil && err != ErrNotFound {
		return "", err
	}

	if err != nil {
		return "", ErrNotFound
	}

	return authAcc.OwnerID, nil
}

// Register registers a new account using the given oauth provider and code. The oauth account will be linked to the account if the registration is successful.
// All for the registration required data will be fetched from the provider. If the account is already registered, the accounts id will just be returned.
// If the registration is success the accounts id will be returned.
func (s *service) Register(provider, code string) (uuid.ID, error) {
	info, err := s.getUserInfo(provider, code)
	if err != nil {
		return "", err
	}

	oAcc, err := s.repo.GetOAuthAccount(provider, info.Subject)
	if err != nil && err != ErrNotFound {
		return "", err
	}

	// OAuthAccount does exist so don't register it and just return the account id
	if err == nil {
		return oAcc.OwnerID, nil
	}

	acc, err := s.accSrv.Create(account.Params{
		Email:      &info.Email,
		GivenName:  &info.GivenName,
		FamilyName: &info.FamilyName,
		Birthday:   &info.BirthDay,
		PictureURL: &info.PictureURL,
	}, info.EmailVerified)

	if err != nil {
		return "", err
	}

	i, err := NewAccount(provider, info.Subject, acc.ID)
	if err != nil {
		return "", err
	}

	if err := s.repo.InsertExtauthAccount(i); err != nil {
		return "", err
	}

	return acc.ID, nil
}

// AuthCodeURL generates a new extauth AuthUrl for the given provider (e.g. google) with the given state.
func (s *service) AuthCodeURL(provider, state string) (string, error) {
	return s.manager.AuthCodeURL(provider, state)
}

// RandomState generates a new random extauth state.
func (s *service) RandomState() (string, error) {
	return multiauth.RandomState()
}

// getUserInfo returns the multiauth userInfo fetched from the given extauth provider using the given code.
func (s *service) getUserInfo(provider, code string) (multiauth.UserInfo, error) {
	info, err := s.manager.Authenticate(provider, code)
	if err != nil {
		return multiauth.UserInfo{}, err
	}

	return info, nil
}
