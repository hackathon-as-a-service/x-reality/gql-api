package extauth

import "gitlab.com/gett-hackathon/x-reality/gql-api/internal/errors"

var (
	// ErrNotFound is returned if the extauth account wasn't found
	ErrNotFound = errors.New(errors.NotFound, "extauth account not found")
)
