package session

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/permission"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/uuid"

	"time"
)

const (
	// Lifetime is the maximal allowed session age.
	Lifetime = time.Hour * 24 * 30

	// LoginSessionLifetime is the maximal allowed login session age.
	LoginSessionLifetime = time.Minute * 30

	// EmailActivationSessionLifetime is the maximal allowed email activation session age.
	EmailActivationSessionLifetime = time.Hour * 12
)

// Session represents a barbra session used for authenticating user requests.
type Session struct {
	*jwt.StandardClaims
}

// Login represents a session used during extauth login/registration to store the state.
type Login struct {
	*jwt.StandardClaims
	Provider string `json:"provider"`
	State    string `json:"state"`
	Register bool   `json:"register"`
}

// EmailVerification represents a session used during the email verification progress
type EmailVerification struct {
	*jwt.StandardClaims
	Email string `json:"email"`
}

// New creates a new session for the given subject
func New(subject uuid.ID) Session {
	return Session{newStandardClaims(subject, Lifetime)}
}

// NewLogin creates a new login session for the given state and provider
func NewLogin(state, provider string, register bool) Login {
	return Login{
		Provider:       provider,
		State:          state,
		StandardClaims: newStandardClaims(permission.UnauthorizedID, LoginSessionLifetime),
		Register:       register,
	}
}

// NewEmailVerification creates a new email email verification session
func NewEmailVerification(subject uuid.ID, email string) EmailVerification {
	return EmailVerification{
		Email:          email,
		StandardClaims: newStandardClaims(subject, EmailActivationSessionLifetime),
	}
}

// newStandardClaims creates new jwt standard claims for the given subject and the given lifeTime
func newStandardClaims(subject uuid.ID, lifeTime time.Duration) *jwt.StandardClaims {
	now := time.Now()

	return &jwt.StandardClaims{
		ExpiresAt: now.Add(lifeTime).Unix(),
		NotBefore: now.Unix(),
		Subject:   subject.String(),
		Id:        uuid.New().String(),
		IssuedAt:  now.Unix(),
	}
}
