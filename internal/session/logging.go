package session

import (
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/gett-hackathon/x-reality/gql-api/internal/logging"
)

// loggingService provides a logging wrapper around Service.
type loggingService struct {
	next Service
	log  logging.Logger
}

// NewLoggingService creates a new logging service.
func NewLoggingService(next Service, log logging.Logger) Service {
	return &loggingService{
		log:  log.WithFields("service", "session"),
		next: next,
	}
}

// Token provides a logging wrapper around service.Token.
func (s *loggingService) Token(ses jwt.Claims) (token string, err error) {
	defer s.log.Debugw("called method Token", "session", ses, "error", err)
	return s.next.Token(ses)
}

// Parse provides a logging wrapper around service.Parse.
func (s *loggingService) Parse(claims jwt.Claims, token string) (err error) {
	defer s.log.Debugw("called method Parse", "claims", claims, "error", err)
	return s.next.Parse(claims, token)
}
