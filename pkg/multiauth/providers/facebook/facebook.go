package facebook

import (
	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/facebook"
)

// client is a facebook based Client implementation.
type client struct {
	*multiauth.ClientBase
}

// New creates a new facebook extauth client.
func New(id, secret, redirectURL string) multiauth.Client {
	return &client{
		ClientBase: multiauth.NewClientBase(&oauth2.Config{
			ClientID:     id,
			ClientSecret: secret,
			Endpoint:     facebook.Endpoint,
			RedirectURL:  redirectURL,
			Scopes:       []string{"email"},
		}),
	}
}

// ID returns the clients ID (facebook in this case).
func (c *client) ID() string {
	return "facebook"
}

// Authenticate authenticates a user using the given code and returns his userinfo.
func (c *client) Authenticate(code string) (multiauth.UserInfo, error) {
	info := UserInfo{}
	if err := c.FetchUserInfo("https://graph.facebook.com/v3.2/me?fields=id,first_name,last_name,picture,email", code, &info); err != nil {
		return multiauth.UserInfo{}, err
	}

	return info.Universal()
}
