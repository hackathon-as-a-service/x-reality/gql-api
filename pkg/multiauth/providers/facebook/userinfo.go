package facebook

import (
	"net/url"
	"time"

	"gitlab.com/gett-hackathon/x-reality/gql-api/pkg/multiauth"
)

// UserInfo represents the userinfos returned by the facebook userinfo endpoint.
type UserInfo struct {
	Subject    string    `json:"id"`
	GivenName  string    `json:"first_name"`
	FamilyName string    `json:"last_name"`
	Email      string    `json:"email"`
	Birthday   time.Time `json:"birthday"`
	Picture    struct {
		Data struct {
			URL string `json:"url"`
		}
	}
}

// Universal returns a multiauth.UserInfo containing all in the given UserInfo present information.
func (u UserInfo) Universal() (multiauth.UserInfo, error) {
	pictureURL, err := url.PathUnescape(u.Picture.Data.URL)
	if err != nil {
		return multiauth.UserInfo{}, err
	}

	return multiauth.UserInfo{
		Subject:       u.Subject,
		EmailVerified: true,
		Email:         u.Email,
		FamilyName:    u.FamilyName,
		GivenName:     u.GivenName,
		PictureURL:    pictureURL,
	}, nil
}
