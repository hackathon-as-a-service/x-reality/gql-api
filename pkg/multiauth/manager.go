package multiauth

import (
	"crypto/rand"
	"encoding/base64"
)

// Manager is used to manage multiauth clients
type Manager struct {
	clients map[string]Client
}

// NewManager creates a new multiauth manager.
func NewManager(clients ...Client) *Manager {
	manager := &Manager{
		clients: make(map[string]Client),
	}

	manager.AddClients(clients...)
	return manager
}

// AddClients adds the given clients to the manager.
func (m *Manager) AddClients(clients ...Client) {
	for _, client := range clients {
		m.clients[client.ID()] = client
	}
}

// GetClient returns the client with the matching id.
func (m *Manager) GetClient(id string) (Client, error) {
	client, ok := m.clients[id]
	if !ok {
		return nil, ErrClientNotFound
	}

	return client, nil
}

// AuthCodeURL creates a authcode url using the client with the given id and the given state.
func (m *Manager) AuthCodeURL(id, state string) (string, error) {
	client, ok := m.clients[id]
	if !ok {
		return "", ErrClientNotFound
	}

	return client.AuthCodeURL(state), nil
}

// Authenticate authenticates the client with the given id and returns the userinfo of the authenticated user.
func (m *Manager) Authenticate(id, code string) (UserInfo, error) {
	client, ok := m.clients[id]
	if !ok {
		return UserInfo{}, ErrClientNotFound
	}

	return client.Authenticate(code)
}

// RandomState creates a new, random oauth state.
func RandomState() (string, error) {
	b := make([]byte, 60)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}

	return base64.URLEncoding.EncodeToString(b), nil
}
