package multiauth

import "errors"

var (
	// ErrTokenInvalid occurs when the given access token is invalid.
	ErrTokenInvalid = errors.New("token invalid")

	// ErrClientNotFound occurs when no client with a matching id was found.
	ErrClientNotFound = errors.New("client not found")
)
