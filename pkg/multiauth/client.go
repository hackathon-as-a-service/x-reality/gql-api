package multiauth

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"time"

	"golang.org/x/oauth2"
)

// Client represents an exauth client.
type Client interface {
	// AuthCodeURL returns the authcodeurl for the given state.
	AuthCodeURL(state string) string

	// Authenticate authenticates a user.
	Authenticate(code string) (UserInfo, error)

	// ID returns the clients ID.
	ID() string
}

// ClientBase contains code shared between all oauth client implementations.
type ClientBase struct {
	config *oauth2.Config
}

// NewClientBase creates a new oauth client base using the given config.
func NewClientBase(config *oauth2.Config) *ClientBase {
	return &ClientBase{
		config: config,
	}
}

// AuthCodeURL returns the authcode url for the given state.
func (c *ClientBase) AuthCodeURL(state string) string {
	return c.config.AuthCodeURL(state)
}

// FetchUserInfo fetches and serializes the given userinfo from the given url using the given code as authentication.
func (c *ClientBase) FetchUserInfo(url, code string, data interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	token, err := c.config.Exchange(ctx, code)
	if err != nil {
		return err
	}

	if !token.Valid() {
		return ErrTokenInvalid
	}

	client := c.config.Client(ctx, token)
	resp, err := client.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	raw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(raw, data)
}
