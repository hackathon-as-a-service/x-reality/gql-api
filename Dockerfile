# Build binary
FROM golang:1.11 AS builder
WORKDIR /root/
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a ./cmd/server

# Get ca-certificates
FROM alpine:3.8 AS alpine
RUN apk add -U --no-cache ca-certificates

# Build image
FROM scratch
COPY --from=alpine /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /root/server .
ENTRYPOINT ["./server"]
